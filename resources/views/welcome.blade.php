@extends('layouts.app') 

@section('content')
<section id="header" class="header-one">
    <div class="container">
        <div class="col-md-offset-1 col-md-10 col-sm-offset-2 col-sm-8">
            <h1 class="wow fadeIn" data-wow-delay="1s"><img src="{{ url('/') }}/img/procare.png" style="height: 10em; width: 10em" /></h1>
            <h1 class="wow fadeInUp" data-wow-delay="1s">PROCARE</h1>
            <h2 class="wow fadeInUp" data-wow-delay="1s" style="color: #000">Setiap bangunan membutuhkan fleksibilitas sekaligus kualitas jasa dalam pemeliharaannya agar dapat terus memberikan
                rasa aman dan nyaman bagi siapapun yang menggunakannya. Untuk itu kami hadir memberikan alternatif solusi
                bagi anda.</h2>
        </div>
    </div>
</section>

<section id="type">
    <div class="type1 wow fadeInUp col-md-6 col-sm-12" data-wow-delay="1.3s">
        <img src="{{ url('/') }}/img/residensial.png" style="height: 15em; width: 15em;" />
        <h1 class="wow fadeIn" data-wow-delay="1s" style="color: #fff">RESIDENSIAL</h1>
    </div>

    <div class="type2 wow fadeInUp col-md-6 col-sm-12" data-wow-delay="1.3s">
        <img src="{{ url('/') }}/img/komersial.png" style="height: 15em; width: 15em;" />
        <h1 class="wow fadeIn" data-wow-delay="1s" style="color: #fff">KOMERSIAL</h1>
    </div>
</section>

<section id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h1 class="wow fadeIn" data-wow-delay="1s" style="color: #194560; padding-top: 35px">JASA PEMELIHARAAN KAMI</h1>
                <div class="iso-section wow fadeInUp" data-wow-delay="1s">
                    <!-- <div class="iso-box-section wow fadeInUp" data-wow-delay="1s"> -->
                    <div class="iso-box-wrapper col4-iso-box">

                        <div class="iso-box col-md-4 col-sm-6">
                            <div class="portfolio-thumb">
                                <img src="{{ url('/') }}/img/ac.png" class="img-responsive" alt="Portfolio">
                                <div class="portfolio-overlay">
                                    <div class="portfolio-item">
                                        <a href="single-project.html"><i class="fa fa-link"></i></a>
                                        <h2>Air Conditioner</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="iso-box col-md-4 col-sm-6">
                            <div class="portfolio-thumb">
                                <img src="{{ url('/') }}/img/bangunan.png" class="img-responsive" alt="Portfolio">
                                <div class="portfolio-overlay">
                                    <div class="portfolio-item">
                                        <a href="single-project.html"><i class="fa fa-link"></i></a>
                                        <h2>Bangunan</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="iso-box col-md-4 col-sm-6">
                            <div class="portfolio-thumb">
                                <img src="{{ url('/') }}/img/kayu.png" class="img-responsive" alt="Portfolio">
                                <div class="portfolio-overlay">
                                    <div class="portfolio-item">
                                        <a href="single-project.html"><i class="fa fa-link"></i></a>
                                        <h2>Kayu</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="iso-box col-md-4 col-sm-6">
                            <div class="portfolio-thumb">
                                <img src="{{ url('/') }}/img/kuas.png" class="img-responsive" alt="Portfolio">
                                <div class="portfolio-overlay">
                                    <div class="portfolio-item">
                                        <a href="single-project.html"><i class="fa fa-link"></i></a>
                                        <h2>Pengecatan</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="iso-box col-md-4 col-sm-6">
                            <div class="portfolio-thumb">
                                <img src="{{ url('/') }}/img/dinding.png" class="img-responsive" alt="Portfolio">
                                <div class="portfolio-overlay">
                                    <div class="portfolio-item">
                                        <a href="single-project.html"><i class="fa fa-link"></i></a>
                                        <h2>Dinding</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="iso-box col-md-4 col-sm-6">
                            <div class="portfolio-thumb">
                                <img src="{{ url('/') }}/img/listrik.png" class="img-responsive" alt="Portfolio">
                                <div class="portfolio-overlay">
                                    <div class="portfolio-item">
                                        <a href="single-project.html"><i class="fa fa-link"></i></a>
                                        <h2>Kelistrikan</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="reason">
    <div class="container">
        <div class="col-md-12 col-sm-12">
            <div class="iso-section wow fadeInUp" data-wow-delay="1s">
                <center>
                    <h1 class="wow fadeIn" data-wow-delay="1s" style="color: #194560; padding-top: 35px">ALASAN MEMILIH KAMI</h1>
                </center>
                <div class="iso-box-wrapper col4-iso-box">

                    <div class="iso-box col-md-4 col-sm-6">
                        <div class="portfolio-thumb">
                            <img src="{{ url('/') }}/img/ac.png" class="img-responsive" alt="Portfolio">
                            <div class="portfolio-overlay">
                                <div class="portfolio-item">
                                    <h2>Air Conditioner</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="iso-box col-md-4 col-sm-6">
                        <div class="portfolio-thumb">
                            <img src="{{ url('/') }}/img/ac.png" class="img-responsive" alt="Portfolio">
                            <div class="portfolio-overlay">
                                <div class="portfolio-item">
                                    <h2>Air Conditioner</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="iso-box col-md-4 col-sm-6">
                        <div class="portfolio-thumb">
                            <img src="{{ url('/') }}/img/ac.png" class="img-responsive" alt="Portfolio">
                            <div class="portfolio-overlay">
                                <div class="portfolio-item">
                                    <h2>Air Conditioner</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection